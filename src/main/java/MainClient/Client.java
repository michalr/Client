package MainClient;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import DAO.WordDAO;

public class Client {
	public static void main(String[] args) {
		ApplicationContext context = new FileSystemXmlApplicationContext("src/main/resources/beans.xml");
		WordDAO dao = context.getBean(DAO.WordDAO.class);
		dao.postWord("{\"word\":\"omnia\"}");
	}
}

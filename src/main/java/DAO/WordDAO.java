package DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import controll.SearchResultController;
import fileOperations.SimilarWordsReader;
import pojos.Word;

public class WordDAO {

	SessionFactory sessionFactory;
	SearchResultController controller = new SearchResultController();

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<Word> getWord(String word) {
		List<Word> list = controller.get("application/json", word).getWords();
		return list;
	}

	public void postWord(String jsonWord) {
		controller.post(jsonWord);
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(controller.createWordFromJson(jsonWord));
		session.getTransaction().commit();
		session.close();
	
	}

	public void addAllWordsToDB() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		List<Word> list = SimilarWordsReader.readAll();
		list.stream().forEach(e -> session.save(e));
		
		session.getTransaction().commit();
		session.close();
	}

}
